# Dokuwiki

This repo maintains docker image with updated version of the Ubuntu, Lighttpd, PHP and DokuWiki.

[Dokuwiki](https://www.dokuwiki.org/dokuwikii) is a wiki application by Andreas Gohr.

This repo is based from the last Dockerfile of [mprasil/dokuwiki](https://hub.docker.com/r/mprasil/dokuwiki) docker image
tagged 2022-07-31a.

# License

All this content is licensed with the same licences of components of other authors.

