

# AUTHOR:         Rodolfo Pilas <rpilas@pil.as>
# DESCRIPTION:    Image with DokuWiki & lighttpd based on Miroslav Prasil's mprasil/dokuwiki image on https://registry.hub.docker.com/u/mprasil/dokuwiki/. 
# TO_BUILD:       docker build -t pilasguru/dokuwiki .
# TO_RUN:         docker run -d -p 80:80 --name wiki pilasguru/dokuwiki

FROM ubuntu:noble
MAINTAINER Rodolfo Pilas <rpilas@pil.as>
ENV DOKUWIKI_VERSION=2024-02-06b
ARG DOKUWIKI_CSUM=c1bb69056155a42b52db53d726ea923b
ARG DOKUWIKI_DEBIAN_PACKAGES=
RUN export DEBIAN_FRONTEND=noninteractive  \
        && apt-get update  \
        && apt-get -y upgrade  \
        && apt-get -y install wget lighttpd php-cgi php-gd php-ldap php-curl php-xml php-mbstring perl-modules-5.38 ${DOKUWIKI_DEBIAN_PACKAGES}  \
        && apt-get clean autoclean  \
        && apt-get autoremove  \
        && rm -rf /var/lib/{apt,dpkg,cache,log}
RUN wget -q -O /dokuwiki.tgz "https://download.dokuwiki.org/src/dokuwiki/dokuwiki-$DOKUWIKI_VERSION.tgz" \
        && if [ "$DOKUWIKI_CSUM" != "$(md5sum /dokuwiki.tgz | awk '{print($1)}')" ];then echo "Wrong md5sum of downloaded file!"; exit 1; fi \
        && mkdir /dokuwiki \
        && tar -zxf dokuwiki.tgz -C /dokuwiki --strip-components 1 ;\
        chown -R www-data:www-data /dokuwiki
ADD dokuwiki.conf /etc/lighttpd/conf-available/20-dokuwiki.conf

RUN lighty-enable-mod dokuwiki fastcgi accesslog
RUN mkdir /var/run/lighttpd  \
        && chown www-data.www-data /var/run/lighttpd
COPY startup.sh /startup.sh

EXPOSE 80
VOLUME [/dokuwiki/data/ /dokuwiki/lib/plugins/ /dokuwiki/conf/ /dokuwiki/lib/tpl/ /var/log/]
ENTRYPOINT ["/startup.sh"]
CMD ["run"]
